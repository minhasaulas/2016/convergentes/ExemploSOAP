package br.edu.up;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService  //JAX-WS SOAP
public class CadastroWS {
	
	public static void main(String[] args) {
		CadastroWS ws = new CadastroWS();
		Endpoint.publish("http://localhost:8080/ws", ws);
		System.out.println("Servidor rodando...");
	}
	
	
	@WebResult(name="produto")
	public List<Produto> listar(){
		
		Produto p = new Produto();
		p.setId(1L);
		p.setNome("Pneu");

		Produto p2 = new Produto();
		p2.setId(2L);
		p2.setNome("Roda");
		p2.setValor(250.00);
		
		List<Produto> lista = new ArrayList<>();
		lista.add(p);
		lista.add(p2);
		
		return lista;
	}

}
