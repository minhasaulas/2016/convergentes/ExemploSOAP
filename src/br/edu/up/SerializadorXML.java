package br.edu.up;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

public class SerializadorXML {

	public static void main(String[] args) throws Exception {

		Produto p = new Produto();
		p.setId(1L);
		p.setNome("Pneu");
		p.setValor(300.00);
		
		JAXBContext ctx = JAXBContext.newInstance(Produto.class);
		Marshaller marshall = ctx.createMarshaller();
		marshall.marshal(p, new File("produto.xml"));
		
		
	}

}
